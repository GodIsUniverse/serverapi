﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerAPI.Models
{
    
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Contact( int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}