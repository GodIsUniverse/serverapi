﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServerAPI.Models;
using System.IO;

namespace ServerAPI.Controllers
{
    [RoutePrefix("Companies")]
    public class ContactController : ApiController
    {
        [HttpGet]
        [Route("CompaniesList")]
        public List<Company> Get(List<string> companyNames)
        {
            using (var context = new TESTDATABASEEntities1())
            {
                if (companyNames == null)
                {
                    var companiesList = context.Companies.ToList();
                    return companiesList;
                }
                var result = context.Companies.Where(x=> companyNames.Contains(x.firmName)).Select(x=>x).ToList();
                return result;
            }
        }

        [HttpGet]
        [Route("{id}")]
        public List<Company> Get(int id)
        {
            using (var context = new TESTDATABASEEntities1())
            {
                var result = context.Companies.Where(x => x.id == id).Select(x => x).ToList();
                return result;
            }
        }

        [HttpPost]
        [Route("postCompany")]
        public string Post(Company company)
        {
            using (var context = new TESTDATABASEEntities1())
            {
                var companySet = context.Set<Company>();             
                                
                if (company != null)
                {
                    companySet.Add(company);
                    context.SaveChanges();
                    return "Success";
                }
                return "Failure";
            }
        }
    }
}
